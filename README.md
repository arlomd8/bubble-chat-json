# **Bubble Chat JSON**

## **ARLO MARIO DENDI (4210181018)**

![BubbleChat GIF](4210181018_ARLO MARIO DENDI.gif)

### Workflow
The program will request to download the JSON file from this [URL](https://5e510330f2c0d300147c034c.mockapi.io/users). If the download succeed, the program will sort the JSON text into arrays using SimpleJSON API. The value of 18th index will be assigned into a variable that will be used as a parameter in creating (instantiate) a class object. The class object will hold the data from 18th index.  Finally, the program will get the value, such name and email from the instance of a class object. 

Note : The index number is based on last three attendee's number. Example : 4210181018 (last three numbers are 018)

### Scripts

- [BubbleChatScript](Bubble Speech/Assets/Scripts/BubbleChat.cs)
- [JSONClass](Bubble Speech/Assets/Scripts/JSON Class.cs)





